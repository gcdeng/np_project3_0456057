#include <windows.h>
#include <list>
using namespace std;

#include "resource.h"

#define SERVER_PORT 5566
#define MAX_MSG 100000
#define MAX_CMD 15000
#define MAX_SERVER 5

#define WM_SOCKET_NOTIFY (WM_USER + 1)

typedef struct server {
	char ip[100];
	int port;
	char fileName[20];
	int csock;
	FILE *testFilePtr;
	char command[MAX_CMD];
	bool valid;
	bool exit;
} Server;

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);

//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;

void initialize_server(Server *server) {
	strcpy(server->ip, "");
	server->port = 0;
	strcpy(server->fileName, "");
	strcpy(server->command, "");
	server->valid = false;
	server->exit = false;
}

int readlineFile(FILE *fd, char *ptr, int maxlen)
{
	int n, rc; char c;
	for (n = 1; n < maxlen; n++) {
		if ((rc = fread(&c, sizeof(char), 1, fd)) == 1) { //use fread
			*ptr = c;
			ptr++;
			if (c == '\n') break;
		}
		else if (rc == 0) {
			if (n == 1) return(0);	//EOF, no data read
			else break;				//EOF, some data was read
		}
		else {
			if (n == 1) return(-1);	//error
			else break;
		}
	}
	*ptr = 0;
	return(n);
}

int readlineSocket(int fd, char *ptr, int maxlen)
{
	int n, rc; char c;
	for (n = 1; n < maxlen; n++) {
		if ((rc = recv(fd, &c, 1, 0)) == 1) { //use recv
			*ptr = c;
			ptr++;
			if (c == '\n') break;
		}
		else if (rc == 0) {
			if (n == 1) return(0);	//EOF, no data read
			else break;				//EOF, some data was read
		}
		else {
			if (n == 1) return(-1);	//error
			else break;
		}
	}
	*ptr = 0;
	return(n);
}

void CGIhtmlTemplate(int ssock, Server servers[]) {
	//send(ssock, "Content-Type: text/html; charset=big5\n\n", strlen("Content-Type: text/html; charset=big5\n\n"), 0);
	//head
	send(ssock, "<html>\r\n", strlen("<html>\r\n"), 0);
	send(ssock, "<head>\r\n", strlen("<head>\r\n"), 0);
	send(ssock, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n", strlen("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n"), 0);
	send(ssock, "<meta http-equiv=\"encoding\" content=\"big5\"/>\r\n", strlen("<meta http-equiv=\"encoding\" content=\"big5\"/>\r\n"), 0);
	send(ssock, "<title>Network Programming Homework 3</title>\r\n", strlen("<title>Network Programming Homework 3</title>\r\n"), 0);
	send(ssock, "<style type='text/css'> xmp {display: inline;} </style>", strlen("<style type='text/css'> xmp {display: inline;} </style>"), 0);
	send(ssock, "</head>\r\n", strlen("</head>\r\n"), 0);
	//body
	send(ssock, "<body bgcolor=#336699>\r\n", strlen("<body bgcolor=#336699>\r\n"), 0);
	send(ssock, "<font face=\"Courier New\" size=2 color=#FFFF99>\r\n", strlen("<font face=\"Courier New\" size=2 color=#FFFF99>\r\n"), 0);
	send(ssock, "<table width=\"800\" border=\"1\">\r\n", strlen("<table width=\"800\" border=\"1\">\r\n"), 0);
	send(ssock, "<tr>\r\n", strlen("<tr>\r\n"), 0);
	//IP td
	for (int i = 0; i < MAX_SERVER; i++) {
		if (strcmp(servers[i].ip, "") != 0) {
			// get host by name
			struct hostent *host;
			host = gethostbyname(servers[i].ip);
			char IPtd[MAX_MSG] = "";
			sprintf(IPtd, "<td>%s</td>", inet_ntoa(*( (struct in_addr *) host->h_addr )));
			send(ssock, IPtd, strlen(IPtd), 0);
		}
		else
			send(ssock, "<td></td>", strlen("<td></td>"), 0);
	}
	send(ssock, "\r\n</tr>\r\n", strlen("\r\n</tr>\r\n"), 0);

	// result td
	send(ssock, "<tr>\r\n", strlen("<tr>\r\n"), 0);
	for (int i = 0; i < MAX_SERVER; i++) {
		char rsltd[MAX_MSG] = "";
		sprintf(rsltd, "<td valign=\"top\" id=\"m%d\"></td>", i);
		send(ssock, rsltd, strlen(rsltd), 0);
	}
	send(ssock, "\r\n</tr>\r\n", strlen("\r\n</tr>\r\n"), 0);
	send(ssock, "</table>\r\n", strlen("</table>\r\n"), 0);
	send(ssock, "</font>\r\n", strlen("</font>\r\n"), 0);
	send(ssock, "</body>\r\n", strlen("</body>\r\n"), 0);
	send(ssock, "</html>\r\n", strlen("</html>\r\n"), 0);
}

// request self server e.g ras, rwg and establish connect
int requestServer(Server *server, HWND hwnd, HWND hwndEdit) {
	struct hostent *host; //pointer to host information entry
	struct sockaddr_in addr; //an Internet endpoint address
	int err;

	// handle parameters
	server->testFilePtr = fopen(server->fileName, "r");
	host = gethostbyname(server->ip);
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(server->port);
	addr.sin_addr = *((struct in_addr *)host->h_addr);

	// create socket
	server->csock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	EditPrintf(hwndEdit, TEXT("=== socket created ===\r\n"));
	
	// establish connect
	if (connect(server->csock, (struct sockaddr *)&addr, sizeof(addr)) == SOCKET_ERROR) {
		if (WSAGetLastError() != WSAEWOULDBLOCK) {
			EditPrintf(hwndEdit, TEXT("=== Error: requestServer connect() failed ===\r\n"));
			//closesocket(server->csock);
			//return TRUE;
		}
		EditPrintf(hwndEdit, TEXT("=== Error: requestServer connect() temporarily failed ===\r\n"));
	}
	else {
		EditPrintf(hwndEdit, TEXT("=== requestServer connect() success ===\r\n"));
	}

	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static SOCKET browserSockfd;
	static struct sockaddr_in sa;

	int err;
	// myself veriable
	static Server servers[MAX_SERVER];
	// connected number
	static int connectCount = 0;
	// count exit number
	static int closeCounter = 0;

	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					// select
					//err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);
					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT);
					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== select FD_ACCEPT success ===\r\n"));
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					//listen socket
					err = listen(msock, 2);
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					WSACleanup();
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			WSACleanup();
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(ssock: %d), List size:%d ===\r\n"), ssock, Socks.size());

					char headersmsg[MAX_MSG];
					char fileData[MAX_MSG];
					char *fileName, *QUERY_STRING;
					int recvStatus;
					FILE *htmlFilePtr;

					memset(headersmsg, 0, MAX_MSG);
					memset(fileData, 0, MAX_MSG);
					recvStatus = recv(ssock, headersmsg, MAX_MSG, 0);
					if (recvStatus < 0) // receive error
						EditPrintf(hwndEdit, TEXT("recv() error\r\n"));
					else if (recvStatus == 0) // receive socket closed
						EditPrintf(hwndEdit, TEXT("Client disconnected!\r\n"));
					else { // headers messages received
						EditPrintf(hwndEdit, TEXT("[Headers]:\r\n%s"), headersmsg);
						//get file name
						strtok(headersmsg, " \t\n"); //GET method
						fileName = strtok(NULL, " \t");
						fileName = &fileName[1]; //remove '/'
						
						EditPrintf(hwndEdit, TEXT("[Workflow]:\r\n"));
						EditPrintf(hwndEdit, TEXT("URL: %s\r\n"), fileName);
						// is html file? (hard code)
						if (strcmp(fileName, "form_get.html") == 0) {
							EditPrintf(hwndEdit, TEXT("=== html ssock: %d, List size:%d ===\r\n"), ssock, Socks.size());
							if ((htmlFilePtr = fopen(fileName, "r")) != NULL) {
								send(ssock, "HTTP/1.0 200 OK\n\n", strlen("HTTP/1.0 200 OK\n\n"), 0); // \n\n is necessary!!!
								// send html to browser client
								while (readlineFile(htmlFilePtr, fileData, 1024)) {
									send(ssock, fileData, strlen(fileData) , 0);
									strcpy(fileData, ""); //initialize
								}
								EditPrintf(hwndEdit, TEXT("html has writen to browser\r\n"));
							}
							else {
								send(ssock, "HTTP/1.0 404 Not Found\n", strlen("HTTP/1.0 404 Not Found\n"), 0); //FILE NOT FOUND
								EditPrintf(hwndEdit, TEXT("html not found\r\n"));
							}
							// close socket
							closesocket(ssock);
							EditPrintf(hwndEdit, TEXT("=== Socket has closed ===\r\n"));
						}
						//maybe it is cgi file or not found
						else {
							fileName = strtok(fileName, "?"); // remove string after '?'
							EditPrintf(hwndEdit, TEXT("filename: %s\r\n"), fileName);
							//is cgi file? (hard code)
							if (strcmp(fileName, "hw3.cgi") == 0) {
								EditPrintf(hwndEdit, TEXT("=== cgi ssock: %d, List size:%d ===\r\n"), ssock, Socks.size());
								browserSockfd = ssock;
								send(ssock, "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=big5\n\n", strlen("HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=big5\n\n"), 0); // \n\n is necessary!!!
								//get query string
								QUERY_STRING = strtok(NULL, " \n"); //get string after ? it is QUERY_STRING
								EditPrintf(hwndEdit, TEXT("QUERY_STRING: %s\r\n"), QUERY_STRING);
								//initialize servers data
								for (int i = 0; i < MAX_SERVER; i++) {
									initialize_server(&servers[i]);
								}
								//assign QUERY_STRING to servers struct variable
								sscanf(QUERY_STRING, "h1=%[^&]&p1=%d&f1=%[^&]&h2=%[^&]&p2=%d&f2=%[^&]&h3=%[^&]&p3=%d&f3=%[^&]&h4=%[^&]&p4=%d&f4=%[^&]&h5=%[^&]&p5=%d&f5=%[^&]",
									servers[0].ip, &servers[0].port, servers[0].fileName,
									servers[1].ip, &servers[1].port, servers[1].fileName,
									servers[2].ip, &servers[2].port, servers[2].fileName,
									servers[3].ip, &servers[3].port, servers[3].fileName,
									servers[4].ip, &servers[4].port, servers[4].fileName);
								//print out to test
								for (int i = 0; i < MAX_SERVER; i++)
								{
									EditPrintf(hwndEdit, TEXT("servers[%d].ip=%s, servers[%d].port=%d, servers[%d].fileName=%s\r\n"), i, servers[i].ip, i, servers[i].port, i, servers[i].fileName);
									if (strcmp(servers[i].fileName, "") != 0) servers[i].valid = true;
								}
								// CGI response html template to browser
								CGIhtmlTemplate(ssock, servers);
								// connect server(ras, rwg)
								for (int i = 0; i < MAX_SERVER; i++)
								{
									if (strcmp(servers[i].ip, "") != 0) {
										requestServer(&servers[i], hwnd, hwndEdit);
										// record connect number
										connectCount++;
									}
								}
								//select(): FD_READ
								for (int i = 0; i < MAX_SERVER; i++)
								{
									if (strcmp(servers[i].ip, "") != 0) {
										err = WSAAsyncSelect(servers[i].csock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
										if (err == SOCKET_ERROR) {
											EditPrintf(hwndEdit, TEXT("=== Error: requestServer select() FD_READ error ===\r\n"));
											closesocket(servers[i].csock);
											return TRUE;
										}
										else {
											EditPrintf(hwndEdit, TEXT("=== requestServer select() FD_READ success ===\r\n"));
										}
									}
								}
								EditPrintf(hwndEdit, TEXT("connectCount: %d\r\n"), connectCount);
								EditPrintf(hwndEdit, TEXT("cgi has writen to browser\r\n"));
							}
							else { //not html or cgi file
								send(ssock, "HTTP/1.0 404 Not Found\n", strlen("HTTP/1.0 404 Not Found\n"), 0); //FILE NOT FOUND
								EditPrintf(hwndEdit, TEXT("not found html or cgi\r\n"));
								// close socket
								closesocket(ssock);
								EditPrintf(hwndEdit, TEXT("=== Socket has closed ===\r\n"));
							}
						}
					}
					break;
				
				case FD_READ:
				//Write your code for read event here.
					EditPrintf(hwndEdit, TEXT("=== FD_READ started ===\r\n"));
					int rcounter;
					int readNum;
					char msg_buf[MAX_MSG];
					char temp_buf[MAX_MSG];
					// initialize
					strcpy(msg_buf, "");
					strcpy(temp_buf, "");

					//which server
					for (rcounter = 0; rcounter < MAX_SERVER; rcounter++) {
						if (servers[rcounter].valid == true && servers[rcounter].csock == wParam)
							break;
					}

					EditPrintf(hwndEdit, TEXT("=== FD_READ: Processing socket %d. ===\r\n"), rcounter);
					// read a line message from self server
					readNum = readlineSocket(servers[rcounter].csock, msg_buf, MAX_MSG);
					
					if(readNum>=0){
						//send message to browser
						EditPrintf(hwndEdit, TEXT("=== readNUM > 0: Read from socket %d: %s ===\r\n"), rcounter, msg_buf);
						// remove \r\n
						for (int i = 0; i < strlen(msg_buf); i++) {
							if (msg_buf[i] == '\r') msg_buf[i] = '\0';
							if (msg_buf[i] == '\n') msg_buf[i] = '\0';
						}
						if (strstr(msg_buf, "%") != NULL) {
							// don't wrap line <br>
							sprintf(temp_buf, "<script>document.all['m%d'].innerHTML += \"<xmp>%s</xmp>\";</script>\r\n", rcounter, msg_buf);
						}
						else {
							// need wrap line <br>
							sprintf(temp_buf, "<script>document.all['m%d'].innerHTML += \"<xmp>%s</xmp><br>\";</script>\r\n", rcounter, msg_buf);
						}
						EditPrintf(hwndEdit, TEXT("=== FD_READ temp_buf: %s ===\r\n"), temp_buf);
						send(browserSockfd, temp_buf, strlen(temp_buf), 0);

						// if have exit
						if (servers[rcounter].exit==true) {
							EditPrintf(hwndEdit, TEXT("=== exit ===\r\n"));
							send(browserSockfd, "=== exit ===", strlen("=== exit ==="), 0);
							// FD CLR
							WSAAsyncSelect(servers[rcounter].csock, hwnd, WM_SOCKET_NOTIFY, 0);
							// close fd
							closesocket(servers[rcounter].csock);
							servers[rcounter].valid = false;
							fclose(servers[rcounter].testFilePtr);
							// check if all over done, close ssock(browser sock fd)
							closeCounter = 0;
							for (int i = 0; i < MAX_SERVER; i++) {
								if (servers[i].exit == true) {
									closeCounter++;
									if (closeCounter == connectCount){
										closesocket(browserSockfd);
										break;
									}
								}
							}
							
							// select(): FD_CLOSE
							/*
							err = WSAAsyncSelect(servers[rcounter].csock, hwnd, WM_SOCKET_NOTIFY, FD_CLOSE);
							if (err == SOCKET_ERROR) {
								EditPrintf(hwndEdit, TEXT("=== Error: select() FD_CLOSE error ===\r\n"));
								closesocket(servers[rcounter].csock);
								return TRUE;
							}
							else {
								EditPrintf(hwndEdit, TEXT("=== select() FD_CLOSE success ===\r\n"));
							}
							*/
						}
						else if (strstr(msg_buf, "%") != NULL) {
							// have %, can start to write
							//select(): FD_WRITE
							err = WSAAsyncSelect(servers[rcounter].csock, hwnd, WM_SOCKET_NOTIFY, FD_WRITE);
							if (err == SOCKET_ERROR) {
								EditPrintf(hwndEdit, TEXT("=== Error: select() FD_WRITE error ===\r\n"));
								closesocket(servers[rcounter].csock);
								return TRUE;
							}
							else {
								EditPrintf(hwndEdit, TEXT("=== select() FD_WRITE success ===\r\n"));
							}
						}
					}
					else {
						EditPrintf(hwndEdit, TEXT("readlineSocket error\n"));
						send(browserSockfd, "\r\n", strlen("\r\n"), 0);
					}
					
					EditPrintf(hwndEdit, TEXT("=== Read no message from self server ===\r\n"));
					break;

				case FD_WRITE:
				//Write your code for write event here
					EditPrintf(hwndEdit, TEXT("=== FD_WRITE started ===\r\n"));
					int wcounter;
					int spaceCounter;
					int cmdNUM;
					char cmd_buf[MAX_CMD];
					strcpy(cmd_buf, "");
					strcpy(temp_buf, "");

					//which server
					for (wcounter = 0; wcounter < MAX_SERVER; wcounter++) {
						if (servers[wcounter].valid == true && servers[wcounter].csock == wParam)
							break;
					}

					// read a line command from test data file
					cmdNUM = readlineFile(servers[wcounter].testFilePtr, cmd_buf, MAX_CMD);

					if (cmdNUM > 0) {
						EditPrintf(hwndEdit, TEXT("=== cmdNUM > 0: Read from file %d: %s ===\r\n"), wcounter, cmd_buf);
						//send command to self server
						send(servers[wcounter].csock, cmd_buf, strlen(cmd_buf), 0);
						//send command to browser
						for (int i = 0; i < strlen(cmd_buf); i++) {
							if (cmd_buf[i] == '\r') cmd_buf[i] = '\0';
							if (cmd_buf[i] == '\n') cmd_buf[i] = '\0';
						}
						sprintf(temp_buf, "<script>document.all['m%d'].innerHTML += \"<b><xmp>%s</xmp></b><br>\";</script>\r\n", wcounter, cmd_buf);
						EditPrintf(hwndEdit, TEXT("=== FD_WRITE temp_buf: %s ===\r\n"), temp_buf);
						send(browserSockfd, temp_buf, strlen(temp_buf), 0);

						// if have exit command
						if (strstr(cmd_buf, "exit") != NULL) {
							EditPrintf(hwndEdit, TEXT("=== exit command ===\r\n"));
							servers[wcounter].exit = true;
						}
						
						// select(): FD_READ
						err = WSAAsyncSelect(servers[wcounter].csock, hwnd, WM_SOCKET_NOTIFY, FD_READ);
						if (err == SOCKET_ERROR) {
							EditPrintf(hwndEdit, TEXT("=== Error: select() FD_READ error ===\r\n"));
							closesocket(servers[wcounter].csock);
							return TRUE;
						}
						else {
							EditPrintf(hwndEdit, TEXT("=== select() FD_READ success ===\r\n"));
						}

					}
					else if (cmdNUM == 0)
						EditPrintf(hwndEdit, TEXT("Read nothing\n"));
					else {
						fclose(servers[wcounter].testFilePtr);
						servers[wcounter].valid = false;
						EditPrintf(hwndEdit, TEXT("Read command error\n"));
					}
					break;

				case FD_CLOSE:
					// when exit
					EditPrintf(hwndEdit, TEXT("=== FD_CLOSE started===\r\n"));
					send(browserSockfd, "=== FD_CLOSE started===", strlen("=== FD_CLOSE started===\r\n"), 0);
					int closeCounter = 0;
					for (int i = 0; i < MAX_SERVER; i++) {
						if (servers[i].exit == true) {
							closeCounter++;
						}
					}
					if (closeCounter == connectCount)
					{
						closesocket(browserSockfd);
					}
					WSAAsyncSelect(servers[rcounter].csock, hwnd, WM_SOCKET_NOTIFY, 0);
					break;
			};
			break;
		
		default:
			return FALSE;


	};

	return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
	TCHAR   szBuffer[100000];
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}